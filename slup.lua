--- Simple Lua permutation.
--
-- @module slup
-- @release 0.2.0
local insert = table.insert
local remove = table.remove
local unpack = unpack or table.unpack

local slup = {}

local function permute_one(list) return list[1] and {{list[1]}} or {{}} end

local function permute_two(list)
  local permutations = {{list[1], list[2]}}
  if list[1] ~= list[2] then
    -- swap
    permutations[2] = {list[2], list[1]}
  end
  return permutations
end

local function insert_member(value, list, return_list)
  for list_index = 1, #list do
    insert(list[list_index], 1, value)
    return_list[#return_list + 1] = list[list_index]
  end
end

local function iter(list)
  local index = 0
  return function ()
      index = index + 1
      return list[index]
    end
end

local function permute_multi(list, count)
  -- กรณีที่ t ไม่ใช่ table
  if type(list) ~= "table" then return {{}} end
  -- กรณีที่มีสมาชิกใน list น้อยกว่า 2 ตัว
  if #list < 2 then return permute_one(list) end
  -- กรณีที่มีสมาชิกใน list แค่ 2 ตัว
  if #list == 2 then return permute_two(list) end
  -- กรณีที่มีสมาชิกใน list มากกว่า 2 ตัว
  local permutations = {}
  local set_of_list_member = {}
  for index = 1, #list do
    if not set_of_list_member[list[index]] then
      set_of_list_member[list[index]] = true
      local sublist = {unpack(list)}
      local source_member_at_index = remove(sublist, index)
      local permutation_of_sublist = permute_multi(sublist, count)
      insert_member(source_member_at_index, permutation_of_sublist, permutations)
    end
  end
  if #permutations[1] == count then
    setmetatable(permutations, {__call = iter})
  end
  return permutations
end

--- สร้าง permutation ที่เป็นไปได้จาก list ที่กำหนด
--
-- @param t list-like table ที่ต้องการหา permutation
-- @return a table of tables ที่เป็น permutation ทั้งหมด
-- @usage slup.permute {1,2,2} --> {{1,2,2},{2,1,2},{2,2,1}}
-- @warning ใช้ recursion หา permutation ดังนั้นถ้ามีสมาชิก 9 ตัวไม่ซ้ำจะได้ permutation 9! หรือ 362880 แบบซึ่งจะใช้เวลาราว 3 วินาทีใน Lua 5.3 และราวเกือบ 2 วินาทีใน LuaJIT
function slup.permute(t)
  return permute_multi(t, #t)
end

--- Iterator แต่ละ permutation ที่เป็นไปได้ของ list
--
-- @param t list-like table ที่ต้องการหา permutation
-- @return iterator ที่ส่งค่าแต่ละ table ที่เป็น permutation ของ list
function slup.iterator(t)
  local permutations = slup.permute(t)
  return permutations()
end

return slup
