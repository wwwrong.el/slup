# Slup

**Simple Lua Permutation**

[Permutation](https://en.wikipedia.org/wiki/Permutation) ของ table คือการสลับลำดับของสมาชิกใน table โดยที่ table ที่มีสมาชิกที่ไม่ซ้ำกันจำนวน n ตัวจะมีรูปแบบการเรียงลำดับที่ไม่ซ้ำกัน n! รูปแบบแต่ถ้ามีสมาชิกใน table ซ้ำกันก็จะมีรูปแบบที่ไม่ซ้ำน้อยลงเช่น 
- ให้ A = {1,2,3,4} จะสามารถสร้าง table จากสมาชิกใน A ที่มีลำดับไม่ซ้ำกันได้ 24 รูปแบบ  
- แต่ถ้าเป็น {1,2,3,3} จะมีได้ 12 รูปแบบ  
- ถ้าเป็น {1,1,2,2} จะมี 6 รูปแบบ  
- และ {1,2,2,2} จะมี 4 รูปแบบ เป็นต้น

ปกติฟังก์ชันหา permutation ส่วนใหญ่จะสร้าง table ที่มีการสลับลำดับสมาชิกทั้งหมดโดยไม่ได้สนใจสมาชิกที่มีค่าซ้ำกันทำให้ได้รูปแบบที่ซ้ำกัน  
แต่ **slup** จะตัดรูปแบบที่ซ้ำออก

## Usage:
### slup.permute()
**[in]:**
```lua
local slup = require("slup")
permutations = slup.permute {1, 2, 2, 4}
for i = 1, #permutations do
  print(("{%s}"):format(table.concat(permutations[i], ",")))
end
print "++++++++++"
local i = 1
for v in permutations() do
  if i == 3 then break end
  print("{"..table.concat(v, ",").."}")
  i = i + 1
end
```
**[out]:**
```
{1,2,2,4}  
{1,2,4,2}  
{1,4,2,2}  
{2,1,2,4}  
{2,1,4,2}  
{2,2,1,4}  
{2,2,4,1}  
{2,4,1,2}  
{2,4,2,1}  
{4,1,2,2}  
{4,2,1,2}  
{4,2,2,1}
++++++++++
{1,2,2,4}  
{1,2,4,2}  
```
### slup.iterator()
**[in]:**
```lua
local permute_iter = require("slup").iterator
for p  in permute_iter{1, 2, 2, 4} do
  print(("{%s}"):format(table.concat(p, ",")))
end
```
**[out]:**
```
{1,2,2,4}  
{1,2,4,2}  
{1,4,2,2}  
{2,1,2,4}  
{2,1,4,2}  
{2,2,1,4}  
{2,2,4,1}  
{2,4,1,2}  
{2,4,2,1}  
{4,1,2,2}  
{4,2,1,2}  
{4,2,2,1}
```
## Limitation:
ใช้ได้กับ List-like table หรือ table ที่มีคีย์เป็นเลขลำดับเรียงกัน และจำกัดที่สมาชิกใน table ต้องเป็นข้อมูลชนิด number, string, boolean หรือข้อมูลที่สามารถใช้ relational operators ในการเปรียบเทียบค่าได้
## New: 28 Jun 2021
* Refactor code.
* List of permutations can iterate itself.
## New: 1 Dec 2020
* Add iterator function.
