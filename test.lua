local inspect = require("inspect")
local permute = require("slup").permute
local iterator = require("slup").iterator

local res = permute({1,2,2,2}) 
print(#res,inspect(res))
assert(#res==4)
 
res = permute({1,2,2,4}) 
print(#res,inspect(res))
assert(#res==12)
 
res = permute({1,1,4,4}) 
print(#res,inspect(res))
assert(#res==6)

res = permute({1,2,3,4}) 
print(#res,inspect(res))
assert(#res==24)

res = permute({1,2,3,4,5,6,7,8}) 
print(inspect(res),#res)
assert(#res==40320)
for v in res() do print(inspect(v)) end

for v in iterator{2,3,4} do print(inspect(v)) end